import numpy as np
import matplotlib.pyplot as pyplot
import init
import evaluation

def afficher (listeCouples, beta, fct, a, b):
    x = np.arange(a, b, 0.01)

    #affichage de la fonction à interpoler
    yf = fct(x)
    pyplot.plot(x, yf, color = 'blue', label="fct à interpoler")
    #affichage du polynôme d'interpolation
    yh = evaluation.horner(listeCouples, beta, x)
    pyplot.plot(x, yh, color = 'red', label="fct d'interpolation")

    #affichage des points d'interpolation
    liste_x = list(map(lambda couple: couple[0], listeCouples))
    liste_y = list(map(lambda couple: couple[1], listeCouples))
    pyplot.plot(liste_x, liste_y, 'ko', markersize = 4, label="point d'interpolation")
    pyplot.legend()
    pyplot.title("Graphique représentant une fonction ainsi que son polynôme d'interpolation")

    pyplot.show()
