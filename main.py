import numpy as np
import init
import descente
import evaluation
import differences_divisees
import affichage
import time
import erreur

#choix de la fonction à interpoler parmi les 4 proposées
print ("Choisir la fonction :")
print (" 1. exp(2x) cos(5x)")
print (" 2. sin(x) + sin(2x) + cos(3x) + cos(4x)")
print (" 3. polynôme : 4x^3 - 48x^2 + 144x")
print (" 4. 1/(1+x^2)")
choixFct = input("Entrez votre choix : ")

#initialisation de l'intervalle
#cas des intervalles par défaut
if (choixFct=='1'):
    a = 0
    b = 2*np.pi
elif (choixFct=='4'):
    a = -5
    b = 5
else:   #si pas d'intervalle par défaut, choix de l'utilisateur
    print ("Choisir l'intervalle d'interpolation [a,b] :")
    a = float(input("Entrez a : "))
    b = float(input("Entrez b : "))

#choix du nombre de points
n = int(input("Choisir le nombre de points d'interpolation : "))

print ("Choisir la méthode :")
print (" 1. Descente")
print (" 2. Différences divisées")
choixMeth = input("Entrez votre choix : ")

#appel de la fct genererPoints avec les attributs correspondants aux choix précédents
if (choixFct=='1'):
    fct = init.fct1
elif (choixFct=='2'):
    fct = init.fct2
elif (choixFct=='3'):
    fct = init.fct3
elif (choixFct=='4'):
    fct = init.fct4

print ("Choisir la répartition des points :")
print (" 1. Équirépartis")
print (" 2. Chebyshev")
choixPoints = input("Entrez votre choix : ")

if (choixPoints == '1'):
    listeCouples = init.genererPoints(n, fct, a, b)
elif (choixPoints == '2'):
    listeCouples = init.genererPointsChebyshev(n, fct, a, b)
start = time.time()
if (choixMeth == '1'):
    beta = descente.methDescente(n, listeCouples)
    #on aura p(x) = sum_{i=0}^{n} beta_i * N_i(x)
elif (choixMeth == '2'):
    beta = differences_divisees.obtenir_beta(listeCouples)
end = time.time()

print ("Temps de calcul =", end - start)
erreur.calcul_erreur(n, a, b, fct, beta, listeCouples)
affichage.afficher (listeCouples, beta, fct, a, b)
