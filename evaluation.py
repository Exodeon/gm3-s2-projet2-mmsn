import init


def horner(listeCouples, beta, a):
    n = len(listeCouples)
    b = beta[-1]
    for i in range(n-2, -1, -1,):
        b = beta[i] + (a - listeCouples[i][0]) * b
    return b

def baseNewton(listeCouples, beta, a):
    n = len(listeCouples)
    N_x = init.calculBaseNewton(listeCouples, a, n)
    p_x = 0
    for i in range(n):
        p_x = p_x + beta[i] * N_x[i]
    return p_x
