import numpy as np


def obtenir_beta(listeCouples):
    n = len(listeCouples)
    beta = np.zeros(n)

    # on initialise la liste des f avec les y
    liste_f = list(map(lambda couple: couple[1], listeCouples))

    # on récupère les x
    liste_x = list(map(lambda couple: couple[0], listeCouples))

    beta[0] = liste_f[0]
    for i in range(1, n):
        nouvelle_liste_f = [(liste_f[j+1] - liste_f[j]) / (liste_x[j+i] - liste_x[j]) for j in range(n - i)]
        beta[i] = nouvelle_liste_f[0]
        liste_f = nouvelle_liste_f

    return beta


