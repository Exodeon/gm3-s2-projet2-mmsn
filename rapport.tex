\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage[left=3cm,right=3cm,top=3cm,bottom=3cm]{geometry}
\usepackage[colorlinks = true,
			linkcolor = blue]{hyperref}
\usepackage{graphicx, float, amssymb, minted}
%opening
\title{~\\~\\~\\~\\~\\~\\~\\~\\ Projet MMSN : Interpolation polynomiale \\~\\~\\~\\~\\~\\}
\author{Marie \textsc{Bochet} et Robin \textsc{Langlois}}

\begin{document}

\maketitle

%\begin{abstract}
%\end{abstract}

\newpage

\tableofcontents
\listoffigures

\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}
Le but de ce projet était de coder un programme permettant d’interpoler une fonction en certains points à l’aide d’un interpolant polynomial construit dans la base de Newton. Pour cela, nous avons implémenté plusieurs méthodes pour la génération des points et pour le calcul des coefficients du polynôme d'interpolation. Dans ce rapport, nous allons étudier les résultats obtenus par l'interpolation de différentes fonctions afin de voir si les interpolations qui en résultent sont acceptables. Nous allons également comparer les différentes méthodes et tenter de conclure sur la meilleure méthode à utiliser pour interpoler une fonction.

\newpage
\section{Etude de l'interpolation de plusieurs fonctions}

\subsection{Polynôme}
Pour commencer, nous allons étudier la fonction polynomiale définie $\forall x \in \mathbb{R}$ par $f_1(x) = 4x^3 - 24x^2 + 144x$. Intuitivement, puisque cette fonction est elle-même un polynôme, nous pouvons nous attendre à ce qu'elle soit facile à interpoler par un polynôme.

Tout d'abord, nous allons commencer par l'interpoler avec 3 points d'interpolation équirépartis, sur l'intervalle $[0;10]$ (voir figure \ref{f1_eq_3pts_0_10}). On remarque que le polynôme d'interpolation est assez éloigné de notre polynôme initial, bien qu'il suive la même tendance. Il semble que trois points d'interpolation ne suffisent pas à interpoler notre polynôme. 
On essaie donc avec 4 points (voir figure \ref{f1_eq_4pts_0_10}). Avec 4 points équirépartis, notre polynôme interpolant est confondu avec $f_1$, ce qui se conforme bien à l'intuition que nous avions. Notre interpolation est donc ici exacte. Cela est cohérent car le polynôme est de degré 3, il existe donc un unique polynôme de degré 3 qui passe par 4 points, on ne peut donc qu'obtenir le même polynôme que $f_1$.

Observons si l'approximation est aussi satisfaisante avec les points de Chebyshev (voir figure \ref{f1_cheb_4pts_0_10}).
Comme nous aurions pu nous y attendre, nous avons également une interpolation exacte avec les points de Chebyshev. On remarque qu'ils sont assez proches des points équirépartis que nous avions pris précédemment, ce qui confirme le résultat.

Nous aimerions maintenant étudier le comportement de cette interpolation lorsque le nombre de points devient grand. Notre interpolation nous donnant un polynôme égal au polynôme d'interpolation, on peut se demander si cela reste vrai avec un grand nombre de points. On teste donc avec 80 points d'interpolation. Pour commencer, on essaie les points équirépartis (voir figure \ref{f1_eq_80pts_0_10}). On voit donc que l'interpolation devient très mauvaise pour ce nombre de points, alors qu'elle était très satisfaisante jusque-là. En effet, si le degré du polynôme d'interpolation est trop grand, des fluctuations apparaissent au bord de l'intervalle, et celles-ci prennent des valeurs de plus en plus extrêmes lorsqu'on augmente le degré. C'est ce qu'on appelle l'effet de Runge. Dès lors, le polynôme interpolant n'est plus égal à $f_1$ : sur les extrémités de l'intervalle, on monte jusqu'à $4 \times 10^{13}$, alors que $f_1$ ne dépasse pas $1000$. De fait, l'échelle du graphique est complètement inadaptée pour observer ce qu'il se passe au milieu de l'intervalle. Cependant, on semble être dans le même ordre de grandeur pour les deux polynômes.

Regardons si nous obtenons la même chose avec les points de Chebyshev (voir figure \ref{f1_cheb_80pts_0_10}). Une nouvelle fois, nous pouvons voir apparaître des phénomènes de Runge. En revanche, il n'y en a cette fois qu'à droite de l'intervalle. On remarque également qu'on atteint des valeurs moins extrêmes que pour les points équirépartis (environ $6 \times 10^7$), mais on a aussi des valeurs négatives du même ordre de grandeur.\\

Tableau des erreurs absolues pour $f_1$ avec 4 points équirépartis (cf figure \ref{f1_eq_4pts_0_10}) :
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|}
\hline
Point & 1.8182 & 3.6364 & 5.4545 & 7.2727 & 9.0909 \\
\hline
Erreur & 2.8428e-14 & 0.0 & 1.2434e-14 & 7.1054e-15 & 0.0 \\
\hline
\end{tabular}
\end{center}
~\\
\color{red}
Avec cette interpolation, nous pouvons voir que l'erreur est très faible. En effet, les erreurs trouvées sont proches de la précision machine. La fonction à interpoler étant elle-même un polynôme, nous pouvions nous attendre à ce que le polynôme d'interpolation soit proche de $f_1$. 
\color{black}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f1_eq_3pts_0_10.png}\\
\caption{Graphique de $f_1$ et de son interpolation polynomiale à partir de 3 points équirépartis}
\label{f1_eq_3pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f1_eq_4pts_0_10.png}\\
\caption{Graphique de $f_1$ et de son interpolation polynomiale à partir de 4 points équirépartis}
\label{f1_eq_4pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f1_cheb_4pts_0_10.png}\\
\caption{Graphique de $f_1$ et de son interpolation polynomiale à partir de 4 points de Chebyshev}
\label{f1_cheb_4pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f1_eq_80pts_0_10.png}\\
\caption{Graphique de $f_1$ et de son interpolation polynomiale à partir de 80 points équirépartis}
\label{f1_eq_80pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f1_cheb_80pts_0_10.png}\\
\caption{Graphique de $f_1$ et de son interpolation polynomiale à partir de 80 points de Chebyshev}
\label{f1_cheb_80pts_0_10}
\end{center}
\end{figure}


\subsection{$e^{2x} \cos(5x)$}
Nous avons ensuite observé l'interpolation polynomiale de la fonction $f_2(x) = e^{2x}\cos(5x)$.
Dans le cas de cette fonction, la qualité de l'interpolation obtenue avec des points équidistants dépend du nombre de points d'interpolation. S'il y a trop peu de points, il n'y a pas assez d'information pour que la fonction d'interpolation soit bonne (voir figure \ref{f2_eq_5pts}).

Ensuite, à partir d'un certain nombre de points (environ 25), la fonction polynomiale est suffisament proche de $f_2$ pour que leurs deux courbes soient confondues graphiquement (voir figure \ref{f2_eq_25pts}). L'interpolation trouvée est donc satisfaisante.

Mais contrairement à ce que l'on pourrait penser, si l'on augmente trop le nombre de points d'interpolation, la qualité de l'interpolation se dégrade (voir figures \ref{f2_eq_62pts} et \ref{f2_eq_75}). Nous voyons donc apparaître de nouveau des effets de Runge.

Pour tenter d'améliorer ces interpolations, nous avons implémenté une méthode permettant de répartir les points de manière plus adaptée grâce au polynôme de Chebyshev. Les points d'interpolation sont déterminés à l'aide d'un calcul de racines d'un polynôme de Chebyshev, ce qui entraîne donc des calculs coûteux. Nous avons pu observer que les résultats obtenus grâce à cette méthode sont meilleurs que ceux vus précédemment. En effet, il existe un intervalle pour lequel, si le nombre de points d'interpolation appartient à cet intervalle, le polynôme obtenu avec les points de Chebyshev est proche de la fonction à interpoler, alors que ce n'est pas le cas avec des points équirépartis, car les points de Chebyshev sont plus concentrés aux extrémités de l'intervalle, ce qui évite dans un premier temps l'apparition de l'effet de Runge. On peut par exemple le voir si on prend 75 points, comme sur les graphiques des figures \ref{f2_eq_75} et \ref{f2_cheb_75}.

Cependant, si l'on augmente trop le nombre de points, l'interpolation se dégrade dans les valeurs proches du bord supérieur de l'intervalle (voir figure \ref{f2_cheb_80}).\\ 

Tableau des erreurs absolues pour $f_2$ avec 80 points de Chebychev (cf figure \ref{f2_cheb_80}) :
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|}
\hline
Point & 1.1424 & 2.2848 & 3.4272 & 4.5696 & 5.7120 \\
\hline
Erreur & 652.38 & 4145.47 & 3200.05 & 10189.22 & 17476.58 \\
\hline
\end{tabular}
\end{center}
~\\
\color{red}
Les erreurs observées pour cette interpolation sont très grandes par rapport aux valeurs de $f_2$. L'interpolation n'est donc pas très bonne.
\color{black}


\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f2_eq_5pts.png}\\
\caption{Graphique de $f_2$ et de son interpolation polynomiale à partir de 5 points équirépartis}
\label{f2_eq_5pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f2_eq_25pts.png}\\
\caption{Graphique de $f_2$ et de son interpolation polynomiale à partir de 25 points équirépartis}
\label{f2_eq_25pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f2_eq_62pts.png}
\caption{Graphique de $f_2$ et de son interpolation polynomiale à partir de 62 points équirépartis}
\label{f2_eq_62pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f2_eq_75.png}
\caption{Graphique de $f_2$ et de son interpolation polynomiale à partir de 75 points équirépartis}
\label{f2_eq_75}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f2_cheb_75.png}\\
\caption{Graphique de $f_2$ et de son interpolation polynomiale à partir de 75 points de Chebyshev}
\label{f2_cheb_75}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f2_cheb_80.png}\\
\caption{Graphique de $f_2$ et de son interpolation polynomiale à partir de 80 points de Chebyshev}
\label{f2_cheb_80}
\end{center}
\end{figure}
~\\


\subsection{$\sin(x) + \sin(2x)  + \cos(3x) + \cos(4x)$}
La troisième fonction que nous allons interpoler est $f_3(x) = \sin(x) + \sin(2x)  + \cos(3x) + \cos(4x)$. C'est un polynôme trigonométrique. Nous allons tout d'abord tenter de l'approcher en prenant 5 points d'interpolation, sur l'intervalle $[0;10]$ (voir figure \ref{f3_eq_5pts_0_10}).
L'interpolation est assez mauvaise, et ne suit que très peu la courbe de la fonction initiale. 5 points d'interpolation ne nous donnent pas assez d'information, et un polynôme de degré insuffisant pour approcher $f_3$. On peut faire la même observation avec 5 points de Chebyshev (voir figure \ref{f3_cheb_5pts_0_10}).
L'allure du polynôme est un peu plus proche de $f_3$ que pour les points équirépartis, mais nous restons relativement loin.

Essayons donc d'augmenter le nombre de points, on teste désormais avec 10 points d'interpolation (voir figure \ref{f3_eq_10pts_0_10}).
Au milieu de l'intervalle, le polynôme d'interpolation commence à bien adhérer à la forme de notre fonction, mais on commence à voir se produire des phénomènes de Runge sur les extrémités, le polynôme commence à y prendre des valeurs plutôt hautes.
Concernant les points de Chebyshev, notre polynôme oscille un peu plus et commence à être en phase avec $f3$, l'interpolation est de plus en plus satisfaisante (voir figure \ref{f3_cheb_10pts_0_10}).

Nous allons maintenant observer les résultats obtenus avec 15 points d'interpolation, sur la figure \ref{f3_eq_15pts_0_10}. 
Le phénomène observé avec 10 points équirépartis s'accentue ici : nous sommes plutôt proches de la fonction à l'intérieur de l'intervalle voulu, mais des phénomènes de Runge encore plus grands se sont formés. 
Au contraire, l'interpolation avec les points de Chebyshev continue de s'améliorer (voir figure \ref{f3_cheb_15pts_0_10}). Regardons le résultat avec 30 points de Chebyshev.
Nous n'arrivons plus à distinguer les deux courbes sur le graphique de la figure \ref{f3_cheb_30pts_0_10}, notre interpolation est devenue extrêmement intéressante. On remarque l'intérêt du placement stratégique des points de Chebyshev. Pour finir, on souhaite observer le comportement lorsque le nombre de points augmente pour les deux méthodes de choix de points. On prend donc 80 points d'interpolation, et on obtient les figures \ref{f3_eq_80pts_0_10} et \ref{f3_cheb_80pts_0_10}.

Pour les deux méthodes, l'interpolation devient très peu exploitable sur les bords de l'intervalle. On remarque une nouvelle fois que pour les points de Chebyshev, cela n'intervient que sur le bord droit de l'intervalle.\\

Tableau des erreurs pour $f_3$ avec 30 points de Chebyshev (cf figure \ref{f3_cheb_30pts_0_10}) :
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|}
\hline
Point & 1.8182 & 3.6364 & 5.4545 & 7.2727 & 9.0909 \\
\hline
Erreur & 6.0436e-05 & 7.7755e-05 & 9.9034e-05 & 3.6502e-07 & 4.2432e-05 \\
\hline
\end{tabular}
\end{center}
~\\
\color{red}
Les erreurs obtenues sont assez faibles, donc cette interpolation est correcte.
\color{black}


\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_eq_5pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 5 points équirépartis}
\label{f3_eq_5pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_cheb_5pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 5 points de Chebyshev}
\label{f3_cheb_5pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_eq_10pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 10 points équirépartis}
\label{f3_eq_10pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_cheb_10pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 10 points de Chebyshev}
\label{f3_cheb_10pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_eq_15pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 15 points équirépartis}
\label{f3_eq_15pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_cheb_15pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 15 points de Chebyshev}
\label{f3_cheb_15pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_cheb_30pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 30 points de Chebyshev}
\label{f3_cheb_30pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_eq_80pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 80 points équirépartis}
\label{f3_eq_80pts_0_10}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f3_cheb_80pts_0_10.png}\\
\caption{Graphique de $f_3$ et de son interpolation polynomiale à partir de 80 points de Chebyshev}
\label{f3_cheb_80pts_0_10}
\end{center}
\end{figure}


\subsection{$\frac{1}{1+x^2}$}
Enfin, nous avons décidé d'étudier la fonction $f_4(x) = \frac{1}{1+x^2}$, dont on a vu en cours qu'elle est difficile à interpoler. Nous allons donc vérifier si cela se confirme. Encore une fois, nous avons augmenté le nombre de points d'interpolation progressivement afin d'observer leur impact sur la qualité du polynôme d'interpolation obtenu avec des points équirépartis.
~\\
Pour commencer, lorsque le nombre de points d'interpolation est faible (voir figure \ref{f4_eq_5pts}), on peut voir que la fonction d'interpolation obtenue est très loin de $f_4$, comme dans les cas précédents. 
Mais contrairement à certaines fonctions précédentes, on ne peut pas trouver un nombre de points d'interpolation pour lequel l'interpolation serait correcte. En effet, on peut voir sur l'illustration en figure \ref{f4_eq_12pts} que l'effet de Runge apparait aux bords de l'intervalle avant même que la fonction d'interpolation s'approche de $f_4$ au milieu de l'intervalle.
Cet effet de Runge ne fait ensuite qu'amplifier (voir figure \ref{f4_eq_20pts}), donc on peut conclure que l'interpolation avec des points équirépartis ne permet pas d'obtenir une bonne approximation de $f_4$.

Nous allons maintenant étudier les résultats obtenus à partir de points de Chebyshev.
Cette fois encore, lorsque le nombre de points d'interpolation est trop faible, comme sur la figure \ref{f4_cheb_5pts}, il n'y a pas assez d'information pour que la fonction d'interpolation soit proche de $f_4$.
Puis, lorsqu'on prend environ 30 points, l'interpolation est très bonne et graphiquement, elle se superpose exactement à $f_4$, comme on peut le voir sur la figure \ref{f4_cheb_30pts}.

Finalement, si on augmente trop le nombre de points (voir figure \ref{f4_cheb_60pts}), on observe l'apparition de l'effet de Runge sur le bord droit, pour les valeurs tendant vers 5.

Nous pouvons donc conclure que cette fonction est très difficile à interpoler, et seule l'utilisation de la répartition des points de Chebyshev avec un nombre de points assez précis nous a permis d'obtenir une bonne approximation de $f_4$.\\

Tableau des erreurs pour $f_4$ avec 60 points de Chebyshev (cf figure \ref{f4_cheb_60pts}):
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|}
\hline
Point &  -3.5366 & -1.0976 & 3.7805 & 4.2683 & 4.7561 \\
\hline
Erreur & 2.2083e-03 & 2.3637e-03 & 7.4029e-03 & 4.5726e-03 & 2.9184e-01 \\
\hline
\end{tabular}
\end{center}

\color{red}
Avec cette interpolation, les erreurs sont globalement correctes, mais elles augmentent lorsqu'on s'approche de la borne supérieure. L'interpolation n'est donc pas satisfaisante sur tout l'intervalle.
\color{black}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f4_eq_5pts.png}\\
\caption{Graphique de $f_4$ et de son interpolation polynomiale à partir de 5 points équirépartis}
\label{f4_eq_5pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f4_eq_12pts.png}\\
\caption{Graphique de $f_4$ et de son interpolation polynomiale à partir de 12 points équirépartis}
\label{f4_eq_12pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f4_eq_20pts.png}\\
\caption{Graphique de $f_4$ et de son interpolation polynomiale à partir de 20 points équirépartis}
\label{f4_eq_20pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f4_cheb_5pts.png}\\
\caption{Graphique de $f_4$ et de son interpolation polynomiale à partir de 5 points de Chebyshev}
\label{f4_cheb_5pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f4_cheb_30pts.png}\\
\caption{Graphique de $f_4$ et de son interpolation polynomiale à partir de 30 points de Chebyshev}
\label{f4_cheb_30pts}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{images/interp_f4_cheb_60pts.png}\\
\caption{Graphique de $f_4$ et de son interpolation polynomiale à partir de 60 points de Chebyshev}
\label{f4_cheb_60pts}
\end{center}
\end{figure}


\newpage
\section{Comparaison des méthodes de descente et de différences divisées}

Dans notre programme d'interpolation, nous avons implémenté deux méthodes différentes permettant de trouver le polynôme d'interpolation : la méthode de descente et celle des différences divisées. Dans cette partie, nous allons étudier la différence d'efficacité de ces deux méthodes.\\

Nous avons tout d'abord observé que les différences entre les résultats obtenus par ces méthodes pour une même fonction à interpoler étaient très minimes, voire inexistantes. C'est donc la différence de temps de calcul qui va nous intéresser pour conclure sur l'efficacité des méthodes.

C'est la raison pour laquelle nous avons construit le tableau suivant, qui contient les temps de calcul du polynôme d'interpolation en fonction de la méthode utilisée (en seconde). Nous avons réalisé tous ces calculs avec des points équirépartis, afin de s'assurer que le placement des points n'entre pas en jeu.
Afin des faciliter la comparaison, nous avons ajouté une ligne dans le tableau contenant le rapport entre les temps de calcul des deux méthodes (descente / différences divisées).\\
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|c|}
\hline
Fonction & $f_1$ & $f_1$ & $f_2$ & $f_2$ & $f_3$ & $f_3$ & $f_4$ & $f_4$\\
Nombre de points & 100 & 250 & 100 & 250 & 100 & 250 & 100 & 250 \\
\hline
Descente & 0,0703 & 0,3066 & 0,0624 & 0,3090 & 0,0763 & 0,3132 & 0,0621 & 0,3037 \\
\hline
Différence divisée & 0,0086 & 0,1163 & 0,0134 & 0,1772 & 0,0194 & 0,1720 & 0,0086 & 0,1019 \\
\hline
\hline
Rapport & 8,17 & 2,64 & 4,66 & 1,74 & 3,93 & 1,82 & 7,22 & 2,98 \\
\hline
\end{tabular}
\end{center}
~\\
L'étude de ces résultats permet tout d'abord d'observer que la méthode des différences divisées est toujours plus rapide que celle de la descente. Cette constatation est cohérente avec les calculs de complexité que nous avions réalisé en classe. En effet, nous avions obtenu une complexité de $8n^2$ pour la méthode de descente, et $\frac{3n^2}{2}$ pour les différences divisées.

Cependant, les rapports des temps de calcul ne sont pas constants et ne permettent donc pas de retrouver précisément le rapport donné par les complexités, qui serait de $\frac{16}{3} \simeq 5,333$. Nous pouvons expliquer cela par plusieurs facteurs. \\
D'une part, nous avons effectué notre calcul de complexité en utilisant des approximations. Nous avons par exemple considéré qu'une addition, une soustraction et une multiplication sont équivalentes en temps de calcul, ce qui n'est pas exact. Nos résultats de complexité sont donc à prendre avec précaution. \\
D'autre part, le nombre de points d'interpolation est probablement trop faible pour que les temps de calcul soient totalement représentatifs, ils ne donnent qu'une tendance globale. Nous n'avons cependant pas jugé cohérent d'augmenter ce nombre de points dans la mesure où l'interpolation obtenue avec notre programme est généralement mauvaise si l'on prend plus de 100 points. Ainsi dans la pratique, nous ne serons pas amenés à utiliser ce programme avec un trop grand nombre de points.\\

Finalement, la méthode des différences divisées est meilleure que celle de descente, mais on ne peut pas conclure sur le rapport de rapidité entre les deux méthodes.


\newpage

\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}
Lors de ce projet, nous avons pu voir que l'interpolation est un outil puissant qui permet d'approximer une fonction de manière relativement précise. Cependant, il faut être prudent quant au choix des points et de leur nombre : avec trop peu de points, nous n'avons pas assez d'information pour avoir une approximation convenable, et avec trop de points, nous nous exposons aux phénomènes de Runge. De plus, plus on augmente le nombre de points, et plus on perd l'intérêt de l'interpolation, qui est de calculer rapidement une fonction par son approximation. Nous avons pu voir que le choix des points de Chebyshev est bien meilleur, bien que ces derniers soient plus coûteux à calculer que les points équirépartis. Cependant, les points équirépartis peuvent suffire pour des fonctions qui varient peu, et qui sont donc simples à interpoler. Enfin, nous avons pu comparer deux méthodes de calcul, avec la méthode de descente et les différences divisées. En faisant les calculs d'erreur, nous avons pu remarquer que les observations que nous avions faites sur les graphiques étaient cohérentes. Cela nous a donc aidé à confirmer les conclusions que nous avions pu tirer sur les différentes interpolations que nous avons effectuées.

Il faut également faire attention aux fonctions que nous voulons interpoler : comme nous avons pu le remarquer, certaines sont délicates à interpoler convenablement, il faudra donc être particulièrement vigilant au choix du nombre de points.

\newpage
\section*{Annexes}
\addcontentsline{toc}{section}{Annexes}

\subsection*{Fichier main.py (programme principal)}
\addcontentsline{toc}{subsection}{Fichier main.py (programme principal)}

\inputminted{python}{main.py}

\subsection*{Fichier init.py (initialisation)}
\addcontentsline{toc}{subsection}{Fichier init.py (initialisation)}

\inputminted{python}{init.py}

\subsection*{Fichier descente.py}
\addcontentsline{toc}{subsection}{Fichier descente.py}

\inputminted{python}{descente.py}

\subsection*{Fichier differences divisees.py}
\addcontentsline{toc}{subsection}{Fichier differences divisees.py}

\inputminted[fontsize=\footnotesize]{python}{differences_divisees.py}

\subsection*{Fichier evaluation.py}
\addcontentsline{toc}{subsection}{Fichier evaluation.py}

\inputminted{python}{evaluation.py}

\subsection*{Fichier affichage.py}
\addcontentsline{toc}{subsection}{Fichier affichage.py}

\inputminted{python}{affichage.py}

\subsection*{Fichier erreur.py}
\addcontentsline{toc}{subsection}{Fichier erreur.py}

\inputminted[fontsize=\footnotesize]{python}{erreur.py}

\end{document}
