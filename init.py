import numpy as np
import numpy.polynomial as nppoly

#implémentation des fonctions à interpoler au choix :
def fct1(x):
    return (np.exp(2*x) * np.cos(5*x))

def fct2(x):
    return (np.sin(x) + np.sin(2*x) + np.cos(3*x) + np.cos(4*x))

def fct3(x): #fonction polymomiale
    return (x*(-2*x + 12)**2)

def fct4(x):
    return 1/(1+x**2)

def genererPoints(nbPoints, fct, a, b):
#génère les xi (equirépartis) et yi associés pour une fonction donnée
#la sortie est une liste contenant les couples (xi, yi)
    listeCouples = []
    #calcul de l'écart à mettre entre les points d'interpolation :
    ecartPoints = (b-a)/(nbPoints-1)
    for i in range(nbPoints):
        xi = a + i*ecartPoints
        yi = fct(xi)
        couple = [xi, yi]
        listeCouples.extend([couple])
    return listeCouples

def genererPointsChebyshev(nbPoints, fct, a, b):
#génère les xi (points de Chebyshev) et yi associés pour une fonction donnée
#la sortie est une liste contenant les couples (xi, yi)
    listeCouples = []

    # on construit T(n+1)
    coefficients_chebyshev = [0] * (nbPoints + 1)
    coefficients_chebyshev[-1] = 1
    poly_chebyshev = nppoly.chebyshev.Chebyshev(coefficients_chebyshev)
    x = 0.5 * (a + b) + 0.5 * (b - a) * poly_chebyshev.roots()

    for i in range(nbPoints):
        yi = fct(x[i])
        couple = [x[i], yi]
        listeCouples.extend([couple])
    return listeCouples


def calculBaseNewton(listeCouples, x, indiceMax):
#calcul des Ni de la base de Newton pour le point x, les i allant de 0 à indiceMax
    N = [1]
    for i in range(indiceMax):
        couple = listeCouples[i]
        N.append(N[i]*(x-couple[0]))
    return N
