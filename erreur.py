import evaluation

def calcul_erreur(n, a , b, fonction, beta, listeCouples):
    print("a =", a)
    print("b =", b)
    # on prend un pas différent pour ne pas retomber sur les points d'interpolation
    # ce qui biaiserait le calcul d'erreur.
    nombre_calculs_erreur = 5
    # on rajoute 0.5 pour avoir un pas différent, même si on a autant de points que de calculs
    pas = (b - a) / (nombre_calculs_erreur + 0.5)
    point = a + pas
    for i in range(nombre_calculs_erreur):
        print("Erreur au point", point, ": ", abs(fonction(point) - evaluation.horner(listeCouples, beta, point)))
        point += pas
