import numpy as np
import init

def initMatrice (n, listeCouples):
#initialisation de la matrice servant à la méthode de descente
    mat = np.zeros((n,n))
    for l in range(n):
        couple = listeCouples[l]
        N = init.calculBaseNewton(listeCouples, couple[0], l)
        for c in range(l+1):
            mat[l,c] = N[c]
    return mat

def descente(M,y):
# Resolution de M beta = y
#  - Entree : M (matrice), y (vecteur)
#  - Sortie : beta (Vecteur)
    (n,p) = M.shape
    beta = np.zeros(n)
    for l in range(n):
        beta[l] = (y[l] - sum([M[l,c] * beta[c] for c in range(l)])) / M[l,l]
    return beta

def methDescente (n, listeCouples):
#fonction appliquant la méthode de descente afin d'obtenir beta
    #initialisation de la méthode de descente de la forme M*beta = y
    M = initMatrice (n, listeCouples)   #initialisation de M
    y = np.zeros(n)    #initialisation de y
    for i in range(n):
        couple = listeCouples[i]
        y[i] = couple[1]
    beta = descente(M, y)

    return beta
